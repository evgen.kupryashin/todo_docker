#!/bin/sh

IFS=$'\n'
FILE_LOG='/var/tmp/freedns/freedns_pages.log'
FILE_TOKENS='/var/tmp/freedns/freedns_tokens'
FILE_IP='/var/tmp/freedns/ip'

IP=$(curl -s -k https://ipinfo.io/ip)
if [ ! -e "$FILE_IP" ]; then
	OLD_IP="undefined"
else
	OLD_IP=$(cat "$FILE_IP")
fi

if [ "$IP" != "$OLD_IP" ]; then
	echo "$IP" > "$FILE_IP"

	# Читаем файл построчно
	while IFS= read -r tokens || [ -n "$tokens" ]; do
		# Удаляем пробелы и комментарии
		tokens=$(echo "$tokens" | tr -d '[:space:]' | sed -e 's/#.*//g')
		if [ -n "$tokens" ]; then
			curl -s -k "http://sync.afraid.org/u/$tokens/" >> "$FILE_LOG" 2>&1
		fi
	done < "$FILE_TOKENS"
fi