Новая сборка приложения **todo**

Состоит из пяти контейнеров:
- контейнер app
- контейнер database
- контейнер adminer
- контейнер dns_updater
- контейнер reverse_proxy

Реализовано:
- С помощью .env файла реализовано автоматическое добавление креденшионалов при создании и инициализации БД и подключения node приложения к БД (добавлена зависимость "dotenv": "^10.0.0" в файл todo\package.json). 

Создание БД (файл data\scripts\initdb.sh):
```
#!/bin/bash
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -d "$POSTGRES_DB"  <<-EOSQL
     CREATE TABLE IF NOT EXISTS task( id SERIAL PRIMARY KEY NOT NULL, task text UNIQUE, status INTEGER DEFAULT 0 );
     CREATE USER "$POSTGRES_USER";
     ALTER DATABASE "$POSTGRES_DB" OWNER TO "$POSTGRES_USER";
     ALTER TABLE task OWNER TO "$POSTGRES_USER";
     ALTER USER "$POSTGRES_USER" WITH ENCRYPTED PASSWORD '$POSTGRES_PASSWORD';
EOSQL

```

Подключение к БД (файл docker-compose.yml):
```
    environment:
      - POSTGRES_USER=${DB_USER}
      - POSTGRES_PASSWORD=${DB_PASSWORD}
      - POSTGRES_DB=${DB}

```

Подключение к БД со стороны node (файл todo\server.js):
```const db = kenx({
  client: "pg",
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB
  }
});
```
- К web-сервисам **app**, **adminer**, **reverse_proxy** привязан letsencrypt сертификат. Реализовано с помощью Traefik
- Контейнер **reverse_proxy** служит как reverse proxy - перенаправлят запросы на веб-сервисы
- Контейнер **dns_updater** служит как dyndns сервис - скрипт внутри контейнера парсит файл с токенами. С помощью curl получает внешний IP-адрес и обращаясь по ссылке с токеном в личный кабинет обновляет IP-адрес. Токены генерируются в Личном Кабинете https://freedns.afraid.org/

![image info](images/freedns.png)

Приложение **todo**
![image info](images/app.png)

Приложение **reverse_proxy**
![image info](images/proxy.png)

Приложение **adminer** окно входа

![image info](images/bd.png)

Приложение **adminer** получение результатов с БД
![image info](images/bd1.png)
